#!/usr/bin/env python3

from __future__ import print_function
import sys
import sqlite3
import random
import twitter
from allagantomestonesbot.generator import is_under_char_limit, duty_suffix
from config import *

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

dryRun = False

def get_unused_tomestone(cursor):
    cursor.execute("select * from tomestones where id in (select id from tomestones where used=0 order by random() limit 1)")
    result = cursor.fetchone()
    if not result:
        eprint("error: no unused tomestone names")
        sys.exit(1)
    return result

def get_unused_duty(cursor):
    cursor.execute("select * from duties where id in (select id from duties where used=0 order by random() limit 1)")
    result = cursor.fetchone()
    if not result:
        eprint("error: no unused duty names")
        sys.exit(1)
    return result

### entry point

conn = sqlite3.connect("bot.db")
c = conn.cursor()

while True:
    whatToGenerate = random.choices(population=["just-tomestone", "next-patch", "new-duty"], weights=[7, 1, 3], k=1)[0]
    usedTomestoneIDS = []
    usedDutyIDS = []
    tweetText = ""
    if whatToGenerate == "just-tomestone" or whatToGenerate == "next-patch":
        tomestone = get_unused_tomestone(c)
        usedTomestoneIDS.append(tomestone[0])
        if whatToGenerate == "just-tomestone":
            tweetText = tomestone[1]
        elif whatToGenerate == "next-patch":
            tweetText = f"The next patch will introduce {tomestone[1]}."
    elif whatToGenerate == "new-duty":
        tomestone = get_unused_tomestone(c)
        duty = get_unused_duty(c)
        usedTomestoneIDS.append(tomestone[0])
        usedDutyIDS.append(duty[0])
        dutysuffix = duty_suffix()
        howmany = random.choice(["10", "25", "30", "50", "75", "100"])
        tweetText = f"Completion of the duty {duty[1]}{dutysuffix} will award {howmany} {tomestone[1]}."
    else:
        eprint(f"warning: option {whatToGenerate} unhandled")
        continue
    if not is_under_char_limit(tweetText):
        continue
    # accepted
    if dryRun:
        print(tweetText)
    else:
        # tweet it
        twitterAPI = twitter.Api(consumer_key=consumer_key, consumer_secret=consumer_secret, access_token_key = access_token, access_token_secret = access_secret)
        status = twitterAPI.PostUpdate(tweetText)
        # OK, mark as used
        for i in usedTomestoneIDS:
            c.execute("UPDATE tomestones SET used=1 WHERE id=?", (i,))
        for i in usedDutyIDS:
            c.execute("UPDATE duties set used=1 WHERE id=?", (i,))
        conn.commit()
    break

# finally
conn.close()
