#!/usr/bin/env python3

from __future__ import print_function
from allagantomestonesbot import reader, generator
import sys
import sqlite3

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

### entry point

### usage: generate.py [tomestone|duty] N
if len(sys.argv) != 3:
    eprint("usage: generate.py [tomestone|duty] N")
    sys.exit(1)

nouns = reader.read_wordlists("wordlists/nouns/")
if not nouns:
    eprint("error: no nouns in wordlists")
    sys.exit(1)

adjectives = reader.read_wordlists("wordlists/adjectives/")
if not adjectives:
    eprint("error: no adjectives in wordlists")
    sys.exit(1)
    
gen = None
if sys.argv[1] == 'tomestone':
    gen = lambda n, a: generator.tomestone(n)
elif sys.argv[1] == 'duty':
    gen = lambda n, a: generator.duty_name(n, a)
else:
    eprint("error: specify one of 'tomestone' or 'duty'")
    sys.exit(1)

count = None
try:
    count = int(sys.argv[2])
except:
    eprint("error: second argument must be numeric")
    sys.exit(1)

if count < 1:
    eprint("error: second argument must be positive")
    sys.exit(1)

# now we can start

conn = sqlite3.connect("bot.db")
c = conn.cursor()

print(f"Generating {count} {sys.argv[1]} names.")
print("Press Y to approve each option, or anything else to reject it.")

numberSoFar = 0
while numberSoFar < count:
    nextOption = gen(nouns, adjectives)
    # check if it exists
    if sys.argv[1] == 'tomestone':
        c.execute("SELECT id FROM tomestones WHERE name=?", (nextOption,))
        if c.fetchone():
            # duplicate
            continue
    elif sys.argv[1] == 'duty':
        c.execute("SELECT id FROM duties WHERE name=?", (nextOption,))
        if c.fetchone():
            # duplicate
            continue
    # present to user
    print(f"{nextOption}   ", end='')
    sys.stdout.flush()
    response = sys.stdin.readline().strip()
    if response == 'y' or response == "Y":
        # commit
        if sys.argv[1] == 'tomestone':
            c.execute("INSERT INTO tomestones (name) VALUES (?)", (nextOption,))
        elif sys.argv[1] == "duty":
            c.execute("INSERT INTO duties (name) VALUES (?)", (nextOption,))
        conn.commit()
        numberSoFar += 1
    else:
        # reject
        continue

# finally
conn.close()
