#!/usr/bin/env python3

from __future__ import print_function
from allagantomestonesbot import reader, generator
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

### entry point

nouns = reader.read_wordlists("wordlists/nouns/")
if not nouns:
    eprint("error: no nouns in wordlists")
    sys.exit(1)

adjectives = reader.read_wordlists("wordlists/adjectives/")
if not adjectives:
    eprint("error: no adjectives in wordlists")
    sys.exit(1)
    
print(generator.tomestone(nouns))
print(generator.duty_name(nouns, adjectives))
