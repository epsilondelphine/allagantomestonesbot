import os

def read_wordlists(listsDir):
    words = []
    with os.scandir(listsDir) as it:
        for entry in it:
            if entry.is_file():
                with open(entry.path, 'r') as f:
                    for line in f:
                        words.append(line.strip().lower())
    # inefficient "remove duplicates"
    return list(set(words))
