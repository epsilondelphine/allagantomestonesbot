import random
from nltk.stem import WordNetLemmatizer
import inflect

def capitalize_first_letter(word):
    if word == "":
        return word
    if word[0].isalpha():
        return word[0].upper() + word[1:]

def is_under_char_limit(string):
    return len(string) <= 240
    
def tomestone(nouns):
    name = capitalize_first_letter(random.choice(nouns))
    return f"Allagan Tomestones of {name}"

def duty_suffix():
    duty_suffixes = ["", " (Hard)", " (Extreme)", " (Savage)", " (Ultimate)"]
    return random.choice(duty_suffixes)
    
def duty_name(nouns, adjectives):
    duty_generators = [dutyname_type1, dutyname_type2, dutyname_type3, dutyname_type4]
    gen = random.choice(duty_generators)
    dutyname = gen(nouns, adjectives)
    return dutyname

def maybe_the():
    the = ["The ", ""]
    return random.choice(the)

## duty name generators

def dutyname_type1(nouns, adjectives):
    # "The? ADJ NOUN"
    adjective = capitalize_first_letter(random.choice(adjectives))
    noun = capitalize_first_letter(random.choice(nouns))
    return f"{maybe_the()}{adjective} {noun}"

def dutyname_type2(nouns, adjectives):
    # "The? ADJ of NOUN"
    wnl = WordNetLemmatizer()
    pl = inflect.engine()
    
    adjective = capitalize_first_letter(random.choice(adjectives))
    noun = wnl.lemmatize(random.choice(nouns)) # singular noun
    noun = capitalize_first_letter(pl.plural(noun)) # plural noun
    return f"{maybe_the()}{adjective} of {noun}"

def dutyname_type3(nouns, adjectives):
    # "The? NOUN's NOUN"
    noun1 = capitalize_first_letter(random.choice(nouns))
    noun2 = capitalize_first_letter(random.choice(nouns))
    return f"{maybe_the()}{noun1}'s {noun2}"

def dutyname_type4(nouns, adjectives):
    # "The NOUN"
    noun = capitalize_first_letter(random.choice(nouns))
    return f"The {noun}"
