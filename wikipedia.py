#!/usr/bin/env python3

## Scrape a wikipedia page for the given part of speech.
## Argument 1: Wikipedia page name to look up (must be quoted)
## Argument 2: part of speech tag to extract
##   - NN = (non-proper) noun
##   - JJ, JJR, JJS = adjective

from __future__ import print_function
import pywikibot
import mwparserfromhell
import sys
from nltk import pos_tag, word_tokenize
import re

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

reValid = re.compile(r"^[\w-]+$")
    
###

pagetitle = sys.argv[1]
posTag = sys.argv[2]

site = pywikibot.Site()
page = pywikibot.Page(site, pagetitle)
if page:
    text = page.text
    stripped_text = mwparserfromhell.parse(text).strip_code()
    tokens = word_tokenize(stripped_text)
    tags = pos_tag(tokens)
    selectedTags = [x[0].lower() for x in tags if x[1] == posTag]
    selectedTags = [x for x in selectedTags if reValid.match(x)]
    # lastly, remove duplicates
    selectedTags = set(selectedTags)
    for x in selectedTags:
        print(x)
else:
    eprint("page doesn't exist")
    sys.exit(1)
